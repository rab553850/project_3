# Creating EC2 Instances

resource "aws_instance" "P3_Instance1" {
  ami                         = var.Instance1_ami
  instance_type               = var.Instance_type1
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.Secgrp.id]
  subnet_id                   = aws_subnet.Public_subnet1.id
  availability_zone           = var.AZ_2a
  key_name                    = "Rabb_keypair"

  tags = {
    Name = "P3_Instance_1"
  }
}

resource "aws_instance" "P3_Instance2" {
  ami                         = var.Instance2_ami
  instance_type               = var.Instance_type2
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.Secgrp.id]
  subnet_id                   = aws_subnet.Public_subnet2.id
  availability_zone           = var.AZ_2b
  key_name                    = "Rabb_keypair"

  tags = {
    Name = "P3_Instance_2"
  }
}
