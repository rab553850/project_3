resource "aws_autoscaling_group" "P3_ASG" {
  name                      = "Autoscaling_group"
  max_size                  = 5
  min_size                  = 1
  desired_capacity          = 4
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  launch_configuration      = aws_launch_configuration.ASG_launch_conf.name
  vpc_zone_identifier       = [aws_subnet.Public_subnet1.id, aws_subnet.Public_subnet2.id]
}    

resource "aws_launch_configuration" "ASG_launch_conf" {
  name          = "ASG_launch"
  image_id      = var.Instance2_ami
  instance_type = var.Instance_type2
  key_name = "Rabb_keypair"
  security_groups = [aws_security_group.Secgrp.id]
  associate_public_ip_address = true
}