# Security group. What is a port. A port allows a flow.
resource "aws_security_group" "Secgrp" {
  name        = "EC2_Secgrp"
  description = "Allow mysql inbound traffic"
  vpc_id      = aws_vpc.Project_3_vpc.id


  ingress {
    description = "Opened ingress HTTP port"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    description = "Opened ingress SSH port"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "allow_tcp"
  }
}

